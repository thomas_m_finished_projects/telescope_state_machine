from time import sleep
import random

class Telescope():
    def __init__(self, ):
        """
        Our telescope start
        """

        self.state = "OFFLINE"
        self.array_size = int(12)
        self.dishes_assigned = int(0)
        self.duration = None

    def enable(self):
        """
        Switches on our radio telescope

        raises RuntimeError if array is not in OFFLINE state
        """
        if self.state != "OFFLINE":
            raise RuntimeError("Cannot enable array. Already ONLINE")

        self.state = "ONLINE"

    def assign_resources(self, dishes):
        """
        Sets number of dishes we need for our scan
        and sets the subarray into an RESOURCING state

        Parameters
        ----------
        dishes : int
            The number of dishes we require for our subarray

        raises ValueError if we request more dishes than are
        available

        raises RuntimeError if the array isn't an ONLINE state
        """
        if self.state != "ONLINE":
            raise RunTimeError("Cannot assign a subarray. Dishes are busy, or OFFLINE")

        dishes_available = self.array_size - self.dishes_assigned
        if dishes > dishes_available:
            raise ValueError("Cannot assign {} dishes. Only {} available.".format(dishes, dishes_available))

        if dishes > 0:
            self.dishes_assigned = dishes
            self.state = "RESOURCED"
        else:
            print("No resources assigned")
            self.state = "ONLINE"

    def configure_scan(self, ra, dec, duration):
        """
        Sets the coordinates of the source and the duration of the scan

        Parameters
        ----------
        ra : int
            The right ascension of the source

        dec : int
            The declination of the source

        duration : float
            The scan duration

        raises ValueError if scan duration exceeds 1 hour or is less than 1 minute

        raises ValueError if ra or dec are not sensible
        """

        ra = float(ra)
        dec = float(dec)

        if self.state != "RESOURCED":
            raise RuntimeError("Cannot configure scan")

        if ra < 0 or ra > 24:
            raise ValueError("RA not valid")

        if dec < -33 or dec > 90:
            raise ValueError("DEC not valid")

        if duration < 10 or duration > 3600:
            raise ValueError("Invalid duration")

        self.duration = duration

        print("Position set - ra: {}, dec {}".format(ra, dec))
        print("Observing for {} s".format(duration))
        print("Slewing to position.....")
        sleep(random.randint(1, 10))

        self.state = "READY"

    def scan(self):
        """
        Carries out the scan, setting the subarray to SCANNING,
        then READY once the scan is complete.

        raises RuntimeError if the subarray is not configured to READY.
        """
        if self.state != "READY":
            raise RuntimeError("Subarray not configured - cannot begin scan")

        self.state = "SCANNING"
        print("Scanning.....")
        sleep(self.duration)
        print("Scanning completed")
        self.state = "READY"

    def release_resources(self):
        """
        Checks the subarray has carried out a scan, then sets
        the subarray ONLINE if so, allowing it to be used in another scan.
        """
        if self.state in ["SCANNING", "ONLINE", "OFFLINE"]:
            # Subarray either isn't resourced or we're in the middle of a scan"
            print("Cannot release resources - state is {}".format(self.state))
        else:
            self.dishes_assigned = 0
            self.state = "ONLINE"

    def disable(self):
        """
        Turns off our radio telescope

        raises RuntimeError if array is not in ONLINE state
        """
        if self.state != "ONLINE":
            raise RuntimeError("Cannot disable array - resources are assigned")

        self.state = "OFFLINE"
