from telescope import Telescope

# Connect to our array
tel = Telescope()
print("Array state is", tel.state)

# Turn our array on
tel.enable()
print("Array state is", tel.state)
print("Dishes assigned:", tel.dishes_assigned)

# Assign 6 dishes
tel.assign_resources(6)
print("Dishes assigned:", tel.dishes_assigned)
print("Array state is", tel.state)

# Configure an observation at some coordinates for 30 seconds
tel.configure_scan(10, 80, 11)
print("Array state is", tel.state)

# Start the scan
tel.scan()
print("Array state is", tel.state)

# Release the subarray
tel.release_resources()
print("releasing subarray")
print("Array state is", tel.state)

# End the session
tel.disable()
print("Disconnecting from array")
print("Array state is", tel.state)

