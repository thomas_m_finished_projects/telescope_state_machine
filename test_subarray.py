from telescope import Telescope
import pytest

def test_connection_to_subarray():
    tel = Telescope()
    assert tel.state == "OFFLINE"
    assert tel.dishes_assigned == 0

def test_transition_to_online():
    tel = Telescope()
    tel.enable()
    assert tel.state == "ONLINE"

def test_enable_with_incorrect_state():
    tel = Telescope()
    tel.state = "READY"
    with pytest.raises(RuntimeError):
        tel.enable()

def test_assign_resources():
    tel = Telescope()
    tel.enable()
    tel.assign_resources(6)
    assert tel.state == "RESOURCED"
    assert tel.dishes_assigned == 6

    tel = Telescope()
    tel.enable()
    tel.assign_resources(12)
    assert tel.state == "RESOURCED"
    assert tel.dishes_assigned == 12

    tel = Telescope()
    tel.enable()
    tel.assign_resources(0)
    assert tel.state == "ONLINE"
    assert tel.dishes_assigned == 0

def test_assign_too_many_dishes():
    tel = Telescope()
    tel.enable()
    with pytest.raises(ValueError):
        tel.assign_resources(13)



